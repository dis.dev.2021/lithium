'use strict';

document.addEventListener("DOMContentLoaded", function() {

    // SVG IE11 support
    svg4everybody();
    
    // Mask input
    $("input[name='phone']").mask("+7(999) 999-9999");

    // Mobile Nav Toggle
    $('.nav__button').on('click', function(){
        $('.nav').toggleClass('open');
    });

    // Modal 
    $('.btn-modal').fancybox({
        autoFocus: false,
    });


    let header = $('.header');
    let $h = header.offset().top;

    $(window).on('scroll', function () {
        if ($(window).scrollTop() > $h) {
            header.addClass('fix-top');
        } else {
            header.removeClass('fix-top');
        }
    });


    $(".btn-scroll").on('click', function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top - 70;
        $('html, body').animate({ scrollTop: destination }, 600);
        return false;
    });

    $(".nav__content--link").on('click', function () {
        $('.nav').removeClass('open');
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top - 70;
        $('html, body').animate({ scrollTop: destination }, 600);
        return false;
    });


    let productSlider = new Swiper('.product__slider', {
        loop: true,
        slidesPerView: 'auto',
        spaceBetween: 20,
        breakpoints: {
            750: {
                spaceBetween: 30,
            },
            1340: {
                spaceBetween: 40,
            },
            1580: {
                spaceBetween: 50,
            },
        }
    });

    let actiontSlider = new Swiper('.actions__slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: '.actions__nav',
        },
        breakpoints: {
            750: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            1024: {
                slidesPerView: 2,
                spaceBetween: 40,
            },
            1340: {
                spaceBetween: 30,
                slidesPerView: 3,
            },
            1580: {
                spaceBetween: 70,
                slidesPerView: 3,
            },
        }
    });


    let commentSlider = new Swiper('.comments__slider', {
        loop: true,
        slidesPerView: 'auto',
        spaceBetween: 24,
        breakpoints: {
            1340: {
                spaceBetween: 30,
            },
            1580: {
                spaceBetween: 50,
            },
        }
,
    });


    // primary slider
    var preloaded = document.querySelector('.preloaded');
    var queue = new createjs.LoadQueue(true,null,true);

    queue.on('fileload', handleLoad, this);

    queue.on('progress', handleProgress, this);

    queue.on('complete', handleComplete, this);

    queue.loadManifest([
        {
            id: 'sf',
            crossOrigin: true,
            type: createjs.Types.IMAGE,
            src: '../img/primary__bg.jpg'
        },
        {
            id: 'hk',
            crossOrigin: true,
            type: createjs.Types.IMAGE,
            src: '../img/primary__bg.jpg'
        },
        {
            id: 'lon',
            crossOrigin: true,
            type: createjs.Types.IMAGE,
            src: '../img/primary__bg.jpg'
        },
        {
            id: 'nyc',
            crossOrigin: true,
            type: createjs.Types.IMAGE,
            src: '../img/primary__bg.jpg'
        },
        {
            id: 'tok',
            crossOrigin: true,
            type: createjs.Types.IMAGE,
            src: '../img/primary__bg.jpg'
        }
    ]);

    function handleProgress(event) {
        console.log(event.progress);
    }

    function handleLoad(event) {
        // console.log(event);
        var img = new Image();
        // img.crossOrigin = 'Anonymous';
        img.src = event.item.src;
        preloaded.appendChild(img);
        // var img = queue.getResult('image',true);
        // preloaded.appendChild(event.result);
    }

    function handleComplete() {
        document.querySelector('.main-container').classList.add('loaded');
        console.log('done');
    }



});